export class Product {
  id: number;
  name: string; //8 characters
  price: number; //positive number
}
